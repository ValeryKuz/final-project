﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="FAQ.aspx.cs" Inherits="FAQ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <header>
        <h1> FAQ </h1>
    </header>
    <div class="help-block">
        <h3> General questions:</h3>
        <p><b>What time does your customer service team operate?</b></p>
        <p>Please feel free to contact our Customer Service Team on 0844 846 0111, 8am to 6pm Monday to Thursday, 8am to 5pm Friday and 9am to 1pm Saturday. We're closed on Christmas Day, Boxing Day and New Years Day.</p>
        <h3>Ordering</h3>
        <p><b>How do I pay for my order?</b></p>
        <p>Payment will be taken when all items in your shopping basket have been processed. Your payment details are automatically encrypted when you enter them. You can pay for your online order using; Visa, MasterCard, Solo, Maestro, Visa Electron or American Express cards.</p>
        <p><b>When will my credit/debit card be charged?</b></p>
        <p>Your credit/debit card will be authorised for the amount of your order at the point of checkout. Your credit card will then be charged for each invoice that is raised from that order until your order has been fulfilled.</p>
        <h3>Managing my Account</h3>
        <p><b>What if I forget my password?</b></p>
        <p>From the home page, click the ´Forgotten your password´ link and enter your email address. We´ll then send you a new password. Once you´ve got your new password, enter it on the sign in page and from ´My Account´ you can change the password to one you´ll easily remember.</p>
        <a href="ForgotPassword.aspx">Click here to get a new password.</a>
        <p><b>Can I cancel my order?</b></p>
        <p>You can cancel your order up until the time it is being prepared for despatch. You can do this online in My Account. It is only possible to cancel the whole order not specific items off your order. If your order has already been despatched it will not be possible to cancel.</p>
        <h3>Despatch and Delivery</h3>
        <p><b>If I place an order, how long does delivery take and how much does it cost?</b></p>
        <p>Our policy is to despatch goods within 24 hours of receiving your order, excluding weekends and bank holidays to UK mainland addresses. Delivery to The Channel Islands, Offshore islands, Highlands, Scottish Islands and certain areas of the UK may take slightly longer to reach you. Orders over £30* to most parts of mainland UK have free delivery. This excludes any large or heavy items. Standard carriage charges apply at £4.95 plus VAT, however there may be additional charges on large furniture and heavy or special order items. Please call customer services on 0844 846 0111 for an individual quote. Please note though that some of our items are sent directly from our suppliers, so are subject to a longer delivery time.</p>
    </div>





</asp:Content>

