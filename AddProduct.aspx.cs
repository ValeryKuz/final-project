﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddProduct : System.Web.UI.Page
{
   public static String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrands();
            BindCategory();
            BindGender();
            ddlSubCategory.Enabled = false;
            ddlGender.Enabled = false;
        }
    }

    private void BindGender()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from Gender", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count != 0)
            {
                ddlGender.DataSource = dt;
                ddlGender.DataTextField = "GenderName";
                ddlGender.DataValueField = "GenderID";
                ddlGender.DataBind();
                ddlGender.Items.Insert(0, new ListItem("- Select -", "0"));
            }
            con.Close();
        }
    }

    private void BindCategory()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from Categories", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count != 0)
            {
                ddlCategory.DataSource = dt;
                ddlCategory.DataTextField = "CatName";
                ddlCategory.DataValueField = "CatID";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("- Select -", "0"));
            }
            con.Close();
        }
    }

    private void BindBrands()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
                SqlCommand cmd = new SqlCommand("select * from Brands", con);     
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                if (dt.Rows.Count != 0)
                {
                    ddlBrands.DataSource = dt;
                    ddlBrands.DataTextField = "Name";
                    ddlBrands.DataValueField = "BrandID";
                    ddlBrands.DataBind();
                    ddlBrands.Items.Insert(0, new ListItem("-Select-", "0"));
                }
                con.Close();
            }
        }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        int MainCategoryID = Convert.ToInt32(ddlCategory.SelectedItem.Value);
        using (SqlConnection con = new SqlConnection(CS))
        {

            SqlCommand cmd = new SqlCommand("select * from SubCategories where MainCatID='" + ddlCategory.SelectedItem.Value + "'", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count != 0)
            {
                ddlSubCategory.DataSource = dt;
                ddlSubCategory.DataTextField = "SubCatName";
                ddlSubCategory.DataValueField = "SubCatID";
                ddlSubCategory.DataBind();
                ddlSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));
                ddlSubCategory.Enabled = true;
            }
            con.Close();
        }
    }

    protected void ddlGender_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from Sizes where BrandID='" + ddlBrands.SelectedItem.Value + "' and CategoryID='" + ddlCategory.SelectedItem.Value + "'and SubCategoryID='" + ddlSubCategory.SelectedItem.Value + "'and GenderID='" + ddlGender.SelectedItem.Value + "'", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count != 0)
            {
                cblSize.DataSource = dt;
                cblSize.DataTextField = "SizeName";
                cblSize.DataValueField = "SizeID";
                cblSize.DataBind();

            }
            else
            {
                cblSize.Items.Clear();
            }
        }
    }

    protected void ddlSubCat_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlSubCategory.SelectedIndex!=0)
        {
            ddlGender.Enabled = true;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("procInsertProducts",con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProdName", txtPName.Text);
            cmd.Parameters.AddWithValue("@ProdPrice", txtPPrice.Text);
            cmd.Parameters.AddWithValue("@ProdSellPrice", txtSellPrice.Text);
            cmd.Parameters.AddWithValue("@ProdBrandID", ddlBrands.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@ProdCatID", ddlCategory.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@ProdSubCatID", ddlSubCategory.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@ProdGenderID", ddlGender.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@ProdDescription", txtDesc.Text);
            cmd.Parameters.AddWithValue("@ProdDetails", txtPDetails.Text);
            cmd.Parameters.AddWithValue("@ProdMaterialCare", txtPMaterialCare.Text);

            if(cbFD.Checked==true)
            {
                cmd.Parameters.AddWithValue("@FreeDelivery", 1.ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@FreeDelivery", 0.ToString());
            }

            if (cb30Ret.Checked == true)
            {
                cmd.Parameters.AddWithValue("@30DayRet", 1.ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@30DayRet", 0.ToString());
            }

            if (cbCOD.Checked == true)
            {
                cmd.Parameters.AddWithValue("@COD", 1.ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@COD", 0.ToString());
            }
            con.Open();
            Int64 PID = Convert.ToInt64(cmd.ExecuteScalar());

            //Insert Size Quantity
            for(int i = 0 ; i < cblSize.Items.Count; i++)
            {
                if(cblSize.Items[i].Selected==true)
                {
                    Int64 SizeID = Convert.ToInt64(cblSize.Items[i].Value);
                    int Quantity = Convert.ToInt32(txtQuantity.Text);

                    SqlCommand cmd2 = new SqlCommand("insert into ProductSizeQuantity values('" + PID + "','" + SizeID + "','" + Quantity + "')", con);
                    cmd2.ExecuteNonQuery();
                }
            }

            //Insert and upload Images
            if(fuImg01.HasFile)
            {
                string SavePath = Server.MapPath("~/Images/ProductImages/ProdID-") + PID;
                if (!Directory.Exists(SavePath))
                {
                    Directory.CreateDirectory(SavePath);
                }
                string Extention = Path.GetExtension(fuImg01.PostedFile.FileName);
                fuImg01.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "-01" + Extention);

                SqlCommand cmd3 = new SqlCommand("insert into ProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "-01" +"','"+ Extention + "')", con);
                cmd3.ExecuteNonQuery();

            }
            if (fuImg02.HasFile)
            {
                string SavePath = Server.MapPath("~/Images/ProductImages/ProdID-") + PID;
                if (!Directory.Exists(SavePath))
                {
                    Directory.CreateDirectory(SavePath);
                }
                string Extention = Path.GetExtension(fuImg01.PostedFile.FileName);
                fuImg02.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "-02" + Extention);

                SqlCommand cmd4 = new SqlCommand("insert into ProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "-02" + "','" + Extention + "')", con);
                cmd4.ExecuteNonQuery();

            }
            if (fuImg03.HasFile)
            {
                string SavePath = Server.MapPath("~/Images/ProductImages/ProdID-") + PID;
                if (!Directory.Exists(SavePath))
                {
                    Directory.CreateDirectory(SavePath);
                }
                string Extention = Path.GetExtension(fuImg01.PostedFile.FileName);
                fuImg03.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "-03" + Extention);

                SqlCommand cmd5 = new SqlCommand("insert into ProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "-03" + "','" + Extention + "')", con);
                cmd5.ExecuteNonQuery();

            }
            if (fuImg04.HasFile)
            {
                string SavePath = Server.MapPath("~/Images/ProductImages/ProdID-") + PID;
                if (!Directory.Exists(SavePath))
                {
                    Directory.CreateDirectory(SavePath);
                }
                string Extention = Path.GetExtension(fuImg01.PostedFile.FileName);
                fuImg04.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "-04" + Extention);

                SqlCommand cmd6 = new SqlCommand("insert into ProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "-04" + "','" + Extention + "')", con);
                cmd6.ExecuteNonQuery();

            }
            if (fuImg05.HasFile)
            {
                string SavePath = Server.MapPath("~/Images/ProductImages/ProdID-") + PID;
                if (!Directory.Exists(SavePath))
                {
                    Directory.CreateDirectory(SavePath);
                }
                string Extention = Path.GetExtension(fuImg01.PostedFile.FileName);
                fuImg05.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "-05" + Extention);

                SqlCommand cmd7 = new SqlCommand("insert into ProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "-05" + "','" + Extention + "')", con);
                cmd7.ExecuteNonQuery();

            }
        }
    }
}