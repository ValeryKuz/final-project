﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindCartNumber();

        if (Session["EMAIL"] != null)
        {
            btnSignUp.Visible = false;
            btnSignIn.Visible = false;
            btnSignOut.Visible = true;
            btnManage.Visible = true;
        }
        else
        {
            btnSignUp.Visible = true;
            btnSignIn.Visible = true;
            btnSignOut.Visible = false;
            btnManage.Visible = false;
        }

    }

    public void BindCartNumber()
    {
        if (Request.Cookies["CartPID"] != null) //Check if Cookies exist, if not create one
        {
            string CookiePID = Request.Cookies["CartPID"].Value.Split('=')[1];
            string[] ProductArray = CookiePID.Split(',');
            int ProductCount = ProductArray.Length;
            pCount.InnerText = ProductCount.ToString();
        }

        else
        {
            pCount.InnerText = 0.ToString();
        }
    }

    protected void btnCart_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cart.aspx");
    }

    protected void btnSignOut_Click(object sender, EventArgs e) //When the User logsout we will redirect to the Deafult page
    {
        Session["EMAIL"] = null;
        Response.Redirect("~/Default.aspx");
    }
}