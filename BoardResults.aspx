﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="BoardResults.aspx.cs" Inherits="BoardResults" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
     <div class="row" style="padding-top: 50px">
        <!--Repeater to show all Products-->
         
           <asp:Repeater ID="rptrBoards" runat="server">
            <ItemTemplate>
                <div class="col-sm-3 col-md-3">
                    <!-- product view is a page for viewing the surfboard -->
                    <a style="text-decoration:none;" href="ProductView.aspx?PID=<%#Eval("SurfboardID") %>">
                        <div class="thumbnail">
                            <img src="Images/ProductImages/ProdID-<%#Eval("SurfboardID") %>/<%#Eval("ImageName") %><%#Eval("Extention") %>" alt="<%#Eval("ImageName") %>" />
                            <div class="caption">
                                <div class="proBrand"><%#Eval("BrandName") %></div>
                                <div class="proName prodNameDesc"><%#Eval("ProdName") %></div>
                                <div class="proPrice"><span class="proOgPrice">$<%#Eval("ProdPrice","{0:n}") %></span> <span class="proPriceDiscount">$<%#Eval("DiscAmount","{0:n}") %> OFF </span> <br /> $<%#Eval("ProdSellPrice","{0:n}") %></div>
                                <div class="test"><span class="testspan" ><b>Height:</b></span><%#Eval("Height") %><br /></div>
                                <div class="test"><span class="testspan" ><b>Width:</b></span><%#Eval("Width") %><br /></div>
                                <div class="test"><span class="testspan" ><b>Volume:</b></span><%#Eval("Volume") %><br /></div>
                                <div class="test2"><span class="testspan2"><br /><b>Description:</b></span><%#Eval("ProdDescription") %></div>
                            </div>
                        </div>
                    </a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>

