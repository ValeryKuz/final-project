﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Data;

public partial class SignUp : System.Web.UI.Page
{

    protected void btSignup_Click(object sender, EventArgs e)
    {
        DateTime dateValidator = new DateTime();
        if (!DateTime.TryParse(tbDob.Text, out dateValidator))
        {
            lblMsg.ForeColor = Color.Red;
            lblMsg.Text = "Enter a valid date by the format.";
        }
        else
        {
            if (tbEmail.Text != "" && tbFname.Text != "" && tbLname.Text != "" && tbPass.Text != "" &&
                tbCPass.Text != "" &&
                (DateTime.Parse(tbDob.Text)) != null && tbCity.Text != "" && tbCountry.Text != "")
            {
                if (tbPass.Text == tbCPass.Text) //When password is confirmed, insert data into DB.
                {
                    String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(CS))

                    {
                        SqlCommand cmd = new SqlCommand("insert into Customers values('" + tbEmail.Text + "','" + tbFname.Text + "','" +
                            tbLname.Text + "','" + tbPass.Text + "','" + DateTime.Parse(tbDob.Text) + "','" +
                            tbCity.Text + "','" + tbCountry.Text + "','U')", con);
                        SqlCommand emailChecker = new SqlCommand("select * from Customers where Email='" + tbEmail.Text +
                            "'", con);
                        con.Open();
                        SqlDataAdapter sda = new SqlDataAdapter(emailChecker);
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        con.Close();


                        if (dt.Rows.Count > 0)
                        {
                            lblMsg.ForeColor = Color.Red;
                            lblMsg.Text = "This email address is already taken.";
                        }
                        else
                        {
                            con.Open();
                            cmd.ExecuteNonQuery();
                            lblMsg.ForeColor = Color.Green;
                            lblMsg.Text = "Registration Successful.";
                            con.Close();
                            Response.Redirect("~/SignIn.aspx");
                        }

                    }

                }
                else
                {
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Text = "Passwords do not match";
                }
            }
            else
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "All Fields are Mandatory";
            }
        }
    }
}