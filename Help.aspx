﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="Help.aspx.cs" Inherits="Help" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <header>
        <h1> Need help? </h1>
    </header>
<div class="help-block">
    <h3>Customer Care</h3>
    <p>0844 846 0111</p>
    <p>Calls will cost 7p per minute plus your telephone company's access charge.</p>
    <p>For callers outside of the UK please use +44 151 670 9200</p>
    <p>Call Centre Opening Hours:</p>
    <p>Monday -Thursday 8am-6pm</p>
    <p>Friday  - 8am-5pm</p>
    <p>Saturday - 9am-1pm </p>
    <p>help@boardseek.com</p>
</div>

</asp:Content>

