﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="Delivery.aspx.cs" Inherits="Delivery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="help-block">
        <h1>Despatch and Delivery</h1>
        <p><b>If I place an order, how long does delivery take and how much does it cost?</b></p>
        <p>Our policy is to despatch goods within 24 hours of receiving your order, excluding weekends and bank holidays to UK mainland addresses. Delivery to The Channel Islands, Offshore islands, Highlands, Scottish Islands and certain areas of the UK may take slightly longer to reach you. Orders over £30* to most parts of mainland UK have free delivery. This excludes any large or heavy items. Standard carriage charges apply at £4.95 plus VAT, however there may be additional charges on large furniture and heavy or special order items. Please call customer services on 0844 846 0111 for an individual quote. Please note though that some of our items are sent directly from our suppliers, so are subject to a longer delivery time.</p>
    </div>
</asp:Content>

