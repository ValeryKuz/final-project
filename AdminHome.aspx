﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="AdminHome.aspx.cs" Inherits="AdminHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderAdmin" runat="Server">
    <h1>Dashboard </h1>
    <h4>Control Panel</h4>
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3 id="txtOrders" runat="server"></h3>
                    <p>Orders</p>
                </div>
                <div class="icon">
                    <i class="fas fa-shopping-bag"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3 id="txtUsers" runat="server"></h3>
                    <p>Total Customers</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3 id="txtBoards" runat="server"></h3>
                    <p>SurfBoards</p>
                </div>
                <div class="icon">
                    <i class="fas fa-tint"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3 id="txtProd" runat="server"></h3>
                    <p>Total Products</p>
                </div>
                <div class="icon">
                    <i class="fab fa-product-hunt"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <h1>Purchases</h1>
    <hr />
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">All Purchases</div>

        <!-- Adding repeater to insert all data from Brands table -->
        <asp:Repeater ID="rptrCategory" runat="server">
            <HeaderTemplate>
                <table class="table">
                    <thead>
                        <tr>
                            <th>PurchaseID</th>
                            <th>CustomerID</th>
                            <th>CartAmount</th>
                            <th>CartDiscount</th>
                            <th>TotalPaid</th>
                            <th>PaymentType</th>
                            <th>PaymentStatus</th>
                            <th>DateOfPurchase</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <!-- Repeating items -->
                <tr>
                    <th><%# Eval ("PurchaseID")%></th>
                    <td><%# Eval ("CustomerID") %></td>
                    <td><%# Eval ("CartAmount","{0:n}") %></td>
                    <td><%# Eval ("CartDiscount","{0:n}") %></td>
                    <td><%# Eval ("TotalPaid","{0:n}") %></td>
                    <td><%# Eval ("PaymentType") %></td>
                    <td><%# Eval ("PaymentStatus") %></td>
                    <td><%# Eval ("DateOfPurchase") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                        </table>
            </FooterTemplate>
        </asp:Repeater>

    </div>
</asp:Content>
