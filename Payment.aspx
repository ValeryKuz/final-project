﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdCartAmount" runat="server" />
    <asp:HiddenField ID="hdCartDiscount" runat="server" />
    <asp:HiddenField ID="hdTotalPaid" runat="server" />
    <asp:HiddenField ID="hdPidSizeID" runat="server" />
    <div class="col-md-9">
        <div class="form-horizontal">
            <h2>Delivery Address</h2>
            <hr />
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" CssClass="col-md-2 control-label" Text="Name"></asp:Label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtName" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" CssClass="text-danger" runat="server" ErrorMessage="Please enter Name" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" CssClass="col-md-2 control-label" Text="Address"></asp:Label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAddress" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" runat="server" ErrorMessage="Please enter Address" ControlToValidate="txtAddress"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" CssClass="col-md-2 control-label" Text="Pin Code"></asp:Label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPinCode" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="text-danger" runat="server" ErrorMessage="Please enter Pin Code" ControlToValidate="txtPinCode"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label ID="Label4" runat="server" CssClass="col-md-2 control-label" Text="Phone Number"></asp:Label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="text-danger" runat="server" ErrorMessage="Please enter Phone Number" ControlToValidate="txtPhone"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="text-danger" ControlToValidate="txtPhone" ErrorMessage="Please enter Valid Number" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3" runat="server" id="divPriceDetails">
        <div style="border-bottom: 1px solid #eaeaec;">
            <h3>Price Details</h3>
            <div>
                <label>Cart Total</label>
                <span class="pull-right priceGray" id="spanCartTotal" runat="server"></span><span class="pull-right priceGray">$</span>
            </div>
            <div>
                <label>Cart Discount</label>
                <span class="pull-right priceGreen" id="spanDiscount" runat="server"></span><span class="pull-right priceGreen">$-</span>
            </div>
        </div>
        <div>
            <div class="proPriceView">
                <label>Total</label>
                <span class="pull-right" id="spanTotal" runat="server"></span><span class="pull-right">$</span>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <h3>Choose Payment Mode</h3>
        <hr />
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#Visa">Pay with VISA</a></li>
            <li><a data-toggle="tab" href="#cards">Credit/Debit Cards</a></li>
            <li><a data-toggle="tab" href="#cod">COD</a></li>
        </ul>
        <div class="tab-content">
            <div id="wallets" class="tab-pane fade in active">
                <div class="form-horizontal">
                    <h3>Visa Details</h3>
                    <hr />
                    <div class="form-group">
                        <asp:Label ID="Label5" runat="server" CssClass="col-md-2 control-label" Text="Card Number"></asp:Label>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtCard" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" CssClass="text-danger" runat="server" ErrorMessage="Please enter Card Number" ControlToValidate="txtCard"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" CssClass="text-danger" ControlToValidate="txtCard" ErrorMessage="Please enter Valid Card Number" ValidationExpression="[0-9]{16}"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label7" runat="server" CssClass="col-md-2 control-label" Text="CSC"></asp:Label>
                        <div class="col-md-2 col-md-2">
                            <asp:TextBox ID="txtCSC" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="text-danger" runat="server" ErrorMessage="Required" ControlToValidate="txtCSC"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" CssClass="text-danger" ControlToValidate="txtCSC" ErrorMessage="Enter Valid Number" ValidationExpression="[0-9]{3}"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label6" runat="server" CssClass="col-md-2 control-label" Text="Expiry Date"></asp:Label>
                        <div class="col-md-2 col-md-2">
                            <asp:TextBox ID="txtValidationDate" CssClass="form-control" runat="server" placeholder="Year-Month-Day"></asp:TextBox>
                            <asp:Label ID="lblMsgDate" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <asp:Button ID="btnPaytm" OnClick="btnVisa_Click" CssClass="btn btn-success" runat="server" Text="Pay with Visa" />
                </div>
            </div>
            <div id="cards" class="tab-pane fade">
                <h3>Menu 1</h3>
                <p>Some content menu 1..</p>
            </div>
            <div id="cod" class="tab-pane fade">
                <h3>Menu 2</h3>
                <p>Some content menu 2..</p>
            </div>
        </div>
    </div>
    <%--    <div>
        <h1>Payment methods:</h1>
        <h3>Debit/Credit Cards</h3>
        <img src="Images/cards.png" />
        <p>Please be sure to provide your exact billing address and telephone number that your credit card has on file for you – this will ensure no delays in processing your order.</p>
        <p><b>We do not store credit card details nor do we share customer details with any 3rd parties.</b></p>
        <h3>PayPal</h3>
        <p>Pay for your item in just a few clicks with PayPal, a leader in secure online payments. No PayPal account required. If you have one already, check out quickly – no need to enter your debit or credit card details.</p>
        <h3>Other payment methods</h3>
        <p>We do accept COD (cash on delivery), company cheque, personal cheque, Postal Orders or any other method not listed here.</p>

    </div>--%>
</asp:Content>

