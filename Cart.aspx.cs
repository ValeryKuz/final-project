﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Cart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCartProducts();
        }
    }

    private void BindCartProducts()
    {
        if (Request.Cookies["CartPID"] != null)
        {
            string CookieData = Request.Cookies["CartPID"].Value.Split('=')[1];
            string[] CookieDataArray = CookieData.Split(',');

            if (CookieDataArray.Length > 0)
            {
                h3NoItems.InnerText = "MY CART (" + CookieDataArray.Length + " Items)";
                DataTable dtBrands = new DataTable();
                Int64 CartTotal = 0;
                Int64 Total = 0;
                Int64 DiscountTotal = 0;

                for (int i = 0; i < CookieDataArray.Length; i++) //Separate each Product+SIze
                {
                    string PID = CookieDataArray[i].ToString().Split('-')[0];
                    string SizeID = CookieDataArray[i].ToString().Split('-')[1];

                    String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(CS))
                    {
                        using (SqlCommand cmd = new SqlCommand("select A.*,dbo.getSizeName(" + SizeID + ") as SizeNamee,"
                            + SizeID + " as SizeIDD,SizeData.Name,SizeData.Extention from Products A cross apply(select top 1 B.Name, Extention from ProductImages B where B.ProdID = A.ProdID) SizeData where A.ProdID="
                            + PID + "", con)) //Passing the Product ID to get the product details from DB by using function
                        {
                            cmd.CommandType = CommandType.Text;
                            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                            {
                                sda.Fill(dtBrands);
                            }
                        }
                    }
                    CartTotal += Convert.ToInt64(dtBrands.Rows[i]["ProdPrice"]);
                    if((dtBrands.Rows[i]["ProdDiscount"])!=DBNull.Value)
                    {
                       DiscountTotal += Convert.ToInt64(dtBrands.Rows[i]["ProdDiscount"]);
                    }
                    else
                    {
                        DiscountTotal += 0;
                    }                    
                }
                Total = CartTotal - DiscountTotal;
                rptrCartProducts.DataSource = dtBrands;
                rptrCartProducts.DataBind();
                divPriceDetails.Visible = true;
                bagIcon.Visible = false;

                spanCartTotal.InnerText = "$ " + CartTotal.ToString();
                spanDiscount.InnerText= "-$ " + DiscountTotal.ToString();
                spanTotal.InnerText= "$ " + Total.ToString();


            }

            else
            {
                h3NoItems.InnerHtml = "Your Shopping Cart is Empty";
                divPriceDetails.Visible = false;
                bagIcon.Visible = true;
            }

        }

        else
        {
            h3NoItems.InnerHtml = "Your Shopping Cart is Empty";
            divPriceDetails.Visible = false;
            bagIcon.Visible = true;
        }
    }

    protected void btnRemoveItem_Click(object sender, EventArgs e)
    {
        string CookiePID = Request.Cookies["CartPID"].Value.Split('=')[1];
        Button btn = (Button)(sender);
        string PIDSIZE = btn.CommandArgument;

        List<String> CookiePIDList = CookiePID.Split(',').Select(i => i.Trim()).Where(i => i != string.Empty).ToList(); //Make string with our PID and Size
        CookiePIDList.Remove(PIDSIZE); //remove our product from the cookies list
        string CookiePIDUpdated = String.Join(",", CookiePIDList.ToArray());

        if (CookiePIDUpdated == "") //Upload again the updated cookies
        {
            HttpCookie CartProducts = Request.Cookies["CartPID"];
            CartProducts.Values["CartPID"] = null;
            CartProducts.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(CartProducts);

        }
        else
        {
            HttpCookie CartProducts = Request.Cookies["CartPID"];
            CartProducts.Values["CartPID"] = CookiePIDUpdated;
            CartProducts.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(CartProducts);

        }
        Response.Redirect("~/Cart.aspx");
    }

    protected void btnBuyNow_Click(object sender, EventArgs e)
    {
        if (Session["EMAIL"] != null)
        {
            Response.Redirect("~/Payment.aspx");
        }

        else
        {
            Response.Redirect("~/SignIn.aspx?rurl=cart");
        }
    }
}