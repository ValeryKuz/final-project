﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="AddSubCat.aspx.cs" Inherits="AddSubCat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderAdmin" Runat="Server">
         <div class="container">
            <div class="form-horizontal">
                <h2>Add Sub Category</h2>
                <hr />
              <div class="form-group">
                <asp:Label ID="Label5" runat="server" CssClass="col-md-2 control-label" Text="Main Category"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlCategory" CssClass="form-control" runat="server"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorCategory" CssClass="text-danger" runat="server" ErrorMessage="Please select Category" ControlToValidate="ddlCategory" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="col-md-2 control-label" Text="Sub Category"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtSbCName" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorSbCName" CssClass="text-danger" runat="server" ErrorMessage="Please enter Sub Category Name" ControlToValidate="txtSbCName"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-success" OnClick="btnAdd_Click" />
                    </div>
                </div>
            </div>
               <h1>All Sub Categories</h1>
            <hr />
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">All Brands</div>

                <!-- Adding repeater to insert all data from Brands table -->
                <asp:Repeater ID="rptrCategory" runat="server">  
                    <HeaderTemplate>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>SubCategory</th>
                                    <th>Category</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate> <!-- Repeating items -->
                        <tr>
                            <th><%# Eval ("SubCatID")%></th>
                            <td><%# Eval ("SubCatName") %></td>
                            <td><%# Eval ("CatName") %></td>
                            <td>Edit</td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>

            </div>
        </div>
</asp:Content>

