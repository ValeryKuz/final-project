﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    protected void btnCart_Click(object sender, EventArgs e)
    {

    }

    protected void btBoardMatch_Click(object sender, EventArgs e)
    {
        String query = "";
        query += "wSize=";
        query += wSize.SelectedItem.Text;
        query += "&wType=";
        query += wType.SelectedItem.Text;
        if (CheckBox1.Checked == true)
        {
            query += "&SurferStyleC1=1";
        }
        else query += "&SurferStyleC1=0";

        if (CheckBox2.Checked == true)
        {
            query += "&SurferStyleC2=1";
        }
        else query += "&SurferStyleC2=0";
        if (CheckBox3.Checked == true)
        {
            query += "&SurferStyleC3=1";
        }
        else query += "&SurferStyleC3=0";
        if (CheckBox4.Checked == true)
        {
            query += "&SurferStyleC4=1";
        }
        else query += "&SurferStyleC4=0";
        if (CheckBox5.Checked == true)
        {
            query += "&SurferStyleC5=1";
        }
        else query += "&SurferStyleC5=0";
        if (CheckBox6.Checked == true)
        {
            query += "&SurferStyleC6=1";
        }
        else query += "&SurferStyleC6=0";
        query += "&SurferStyle1=" + CheckBox1.Text;
        query += "&SurferStyle2=" + CheckBox2.Text;
        query += "&SurferStyle3=" + CheckBox3.Text;
        query += "&SurferStyle4=" + CheckBox4.Text;
        query += "&SurferStyle5=" + CheckBox5.Text;
        query += "&SurferStyle6=" + CheckBox6.Text;

        query += "&trickSuitable=";
        query += trickSuitable.SelectedItem.Text;

        query += "&skillReq=";
        query += skillReq.SelectedItem.Text;

        query += "&tbLweight=";
        query += tbLweight.Text;


        //var checkList = new List<String>();

        //if (CheckBox1.Checked == true) checkList.Add(CheckBox1.Text); else checkList.Add("#" + CheckBox1.Text);
        //if (CheckBox2.Checked == true) checkList.Add(CheckBox2.Text); else checkList.Add("#" + CheckBox2.Text);
        //if (CheckBox3.Checked == true) checkList.Add(CheckBox3.Text); else checkList.Add("#" + CheckBox3.Text);
        //if (CheckBox4.Checked == true) checkList.Add(CheckBox4.Text); else checkList.Add("#" + CheckBox4.Text);
        //if (CheckBox5.Checked == true) checkList.Add(CheckBox5.Text); else checkList.Add("#" + CheckBox5.Text);
        //if (CheckBox6.Checked == true) checkList.Add(CheckBox6.Text); else checkList.Add("#" + CheckBox6.Text);
        //var checkboxesarr = checkList.ToArray();
        ////selector functions as a string to add variables to its check list from the array, as well as other checks - wave size and waves suitable...
        //string Selector = "select * from Surfboards where ";
        //for (int i = 0; i < checkboxesarr.Length; i++)
        //{
        //    if (checkboxesarr[i].StartsWith("#"))
        //        Selector += "SurferStyle not like '%" + checkboxesarr[i].Substring(1) + "%' and ";
        //    else
        //        Selector += "SurferStyle like '%" + checkboxesarr[i] + "%' and ";
        //}
        //Selector += "WSizeSuitable like '%" + wSize.SelectedItem.Value +
        // "%' and wTypeSuitable like '%" + wType.SelectedItem.Value + "%'";
        //Selector += " and TrickSuitable like '%" + trickSuitable.SelectedItem.Value + "%'";
        //Selector += " and SkillReq like '%" + skillReq.SelectedItem.Value + "%'";
        ////need to check weight based on algorithm
        ////returns only top 4 surfboards:
        ////Selector += "top 4";        

        Response.Redirect("~/BoardResults.aspx?" + query);
    }

}