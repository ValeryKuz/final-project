﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BoardResults : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindProductRepeater();
        }
    }

    private void BindProductRepeater()
    {
        var queryString = Request.QueryString;
        String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;
        using (SqlConnection con = new SqlConnection(CS))
        {
            //selector functions as a string to add variables to its check list from the array, as well as other checks - wave size and waves suitable...
            string Selector = "select  A.*, B.*, C.Name as BrandName, PI.Name as ImageName, PI.Extention as Extention, B.ProdPrice-B.ProdSellPrice as DiscAmount from Surfboards A join ProductImages PI on (A.SurfboardID = PI.ProdID) join Products B on (A.SurfboardID = B.ProdID) join Brands C on(B.ProdBrandID = C.BrandID) where ";
            Selector += "WSizeSuitable LIKE '%" + queryString["wSize"] + "%' ";
            Selector += "AND WTypeSuitable LIKE '%" + queryString["wType"] + "%' ";
            Selector += "AND TrickSuitable LIKE '%" + queryString["trickSuitable"] + "%' ";
            Selector += "AND SkillReq LIKE '%" + queryString["skillReq"] + "%' ";

            Selector += "AND SurferStyle ";
            Selector += queryString["SurferStyleC1"]=="1" ? "LIKE" : "NOT LIKE";
            Selector += " '%" + queryString["SurferStyle1"] + "%' ";

            Selector += "AND SurferStyle ";
            Selector += queryString["SurferStyleC2"] == "1" ? "LIKE" : "NOT LIKE";
            Selector += " '%" + queryString["SurferStyle2"] + "%' ";

            Selector += "AND SurferStyle ";
            Selector += queryString["SurferStyleC3"] == "1" ? "LIKE" : "NOT LIKE";
            Selector += " '%" + queryString["SurferStyle3"] + "%' ";

            Selector += "AND SurferStyle ";
            Selector += queryString["SurferStyleC4"] == "1" ? "LIKE" : "NOT LIKE";
            Selector += " '%" + queryString["SurferStyle4"] + "%' ";

            Selector += "AND SurferStyle ";
            Selector += queryString["SurferStyleC5"] == "1" ? "LIKE" : "NOT LIKE";
            Selector += " '%" + queryString["SurferStyle5"] + "%' ";

            Selector += "AND SurferStyle ";
            Selector += queryString["SurferStyleC6"] == "1" ? "LIKE" : "NOT LIKE";
            Selector += " '%" + queryString["SurferStyle6"] + "%' ";


            var weight = Convert.ToInt64(queryString["tbLweight"]);
            var volume = weight / 2.74;
            if ((queryString["SurferStyleC1"] == "1" && queryString["SurferStyleC2"] == "1") || (queryString["SurferStyleC1"] == "1" && queryString["SurferStyleC3"] == "1") ||( queryString["SurferStyleC2"] == "1" && queryString["SurferStyleC3"] == "1"))
            {
                volume = weight;
                Selector += "AND (Volume BETWEEN " + Convert.ToString(volume - 5) + " AND " + Convert.ToString(volume + 40) + ") ";
            }
            else
            {
                Selector += "AND (Volume BETWEEN " + Convert.ToString(volume - 1) + " AND " + Convert.ToString(volume + 1) + ") ";
            }
            //"select A.*, B.*, C.Name as BrandName, PI.Name as ImageName, PI.Extention as Extention, B.ProdPrice-B.ProdSellPrice as DiscAmount from Surfboards A left join ProductImages PI on (A.SurfboardID = PI.ProdID) left join Products B on (A.SurfboardID = B.ProdID) left join Brands C on(B.ProdBrandID = C.BrandID) where WSizeSuitable LIKE '%Waist to chest high%' AND WTypeSuitable LIKE '%Soft and mushy%' AND TrickSuitable LIKE '%Catch more Waves%' AND SkillReq LIKE '%Beginner%' AND SurferStyle LIKE '%Simple Cruising%' AND SurferStyle NOT LIKE '%On the Nose%' AND SurferStyle LIKE '%Wide Carves%' AND SurferStyle NOT LIKE '%Fast Cuts %' AND SurferStyle NOT LIKE '%Big Airs%' AND SurferStyle NOT LIKE '%Getting Barreled%' AND (Volume BETWEEN 68 AND 113) "
            SqlCommand cmd = new SqlCommand(Selector, con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            rptrBoards.DataSource = dt;
            rptrBoards.DataBind();
        }

    }
}
