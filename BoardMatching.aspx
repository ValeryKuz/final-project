﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="BoardMatching.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- Board Match Start -->
    <div class="container col-sm-4">
        <h3>Board Matcher</h3>
        <h4>The best way to find a board that fits your needs.</h4>
        <asp:Label ID="lblMsg" runat="server"></asp:Label>
        <div class="center-page">
            <label class="col-xs-11">What size of wave will you ride this board on?</label>
            <div class="col-xs-11">
                <%--<asp:RadioButton ID="RadioButton1" runat="server" Text="Knee to waist high" GroupName="wSize" value="1"/>
                        <asp:RadioButton ID="RadioButton2" runat="server" Text="Waist to chest high" GroupName="wSize" value="2"/>
                        <asp:RadioButton ID="RadioButton3" runat="server" Text="Chest to head high" GroupName="wSize" value="3"/>
                        <asp:RadioButton ID="RadioButton4" runat="server" Text="Overhead" GroupName="wSize" value="4"/>
                        <asp:RadioButton ID="RadioButton5" runat="server" Text="Double overhead plus" GroupName="wSize" value="5"/>--%>
                <asp:DropDownList ID="wSize" class="form-control" runat="server">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Knee to waist high</asp:ListItem>
                    <asp:ListItem Value="2">Waist to chest high</asp:ListItem>
                    <asp:ListItem Value="3">Chest to head high</asp:ListItem>
                    <asp:ListItem Value="4">Overhead</asp:ListItem>
                    <asp:ListItem Value="5">Double overhead plus</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorwSize" CssClass="text-danger" runat="server" ErrorMessage="Please choose wave size" ControlToValidate="wSize"></asp:RequiredFieldValidator>
            </div>
            <label class="col-xs-11">What type of wave do you ride?</label>
            <div class="col-xs-11">
                <asp:DropDownList ID="wType" class="form-control" runat="server">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Soft and mushy</asp:ListItem>
                    <asp:ListItem Value="2">Rolling</asp:ListItem>
                    <asp:ListItem Value="3">Pitching and fast breaking</asp:ListItem>
                    <asp:ListItem Value="4">Hollow and barrelish</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" runat="server" ErrorMessage="Please choose wave type" ControlToValidate="wType"></asp:RequiredFieldValidator>
            </div>
            <label class="col-xs-11">What is your surfing style?</label>
            <div class="col-xs-11">
                <asp:CheckBox ID="CheckBox1" name="styleCB" runat="server" Text="Simple Cruising" /><br />
                <asp:CheckBox ID="CheckBox2" name="styleCB" runat="server" Text="On the Nose" />
                <br />
                <asp:CheckBox ID="CheckBox3" name="styleCB" runat="server" Text="Wide Carves" />
                <br />
                <asp:CheckBox ID="CheckBox4" name="styleCB" runat="server" Text="Fast Cuts & Snaps" />
                <br />
                <asp:CheckBox ID="CheckBox5" name="styleCB" runat="server" Text="Big Airs" />
                <br />
                <asp:CheckBox ID="CheckBox6" name="styleCB" runat="server" Text="Getting Barreled" />

            </div>
            <label class="col-xs-11">What do you wish to accomplish?</label>
            <div class="col-xs-11">
                <asp:DropDownList ID="trickSuitable" class="form-control" runat="server">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Learn to surf</asp:ListItem>
                    <asp:ListItem Value="2">Catch more Waves</asp:ListItem>
                    <asp:ListItem Value="3">Nail more tricks</asp:ListItem>
                    <asp:ListItem Value="4">Try something CRAZY!</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="text-danger" runat="server" ErrorMessage="Please select achievment " ControlToValidate="trickSuitable"></asp:RequiredFieldValidator>
            </div>
            <label class="col-xs-11">How skilled are you?</label>
            <div class="col-xs-11">
                <asp:DropDownList ID="skillReq" class="form-control" runat="server">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Wishing to start</asp:ListItem>
                    <asp:ListItem Value="2">Beginner</asp:ListItem>
                    <asp:ListItem Value="3">Intermediate</asp:ListItem>
                    <asp:ListItem Value="4">Advanced</asp:ListItem>
                    <asp:ListItem Value="4">Sponsored</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" CssClass="text-danger" runat="server" ErrorMessage="Please choose your skill level " ControlToValidate="skillReq"></asp:RequiredFieldValidator>
            </div>

            <label class="col-xs-11">Enter your weight in kilo.</label>
            <div class="col-xs-11">
                <asp:TextBox ID="tbLweight" runat="server" class="form-control" placeholder="Your weight" CausesValidation="True" type="number" min="25" max="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="text-danger" runat="server" ErrorMessage="Please type your weight " ControlToValidate="tbLweight"></asp:RequiredFieldValidator>
            </div>
            <div class="col-xs-11 space-vert">
                <asp:Button ID="btBoardMatch" runat="server" class="btn btn-success" Text="Match a board" OnClick="btBoardMatch_Click" />
            </div>
        </div>

    </div>
    <div class="row col-sm-6" style="float: right;padding-top: 30px;">
        <div class="thumbnail thumbnailboardImg text-center">
            <img src="Images/BoardMatcher.jpg" class="img-responsive"/>
            <div class="captionboardImg">
                <p>Our system will match a board that fits your needs.</p>
            </div>
        </div>
    </div>
    <!-- Sign Up end -->
</asp:Content>

