﻿<%@ Page Title="Cart" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="Cart.aspx.cs" Inherits="Cart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="padding-top: 20px;">
        <div class="col-md-9">
            <h3><span runat="server" id="h3NoItems"></span><i id="bagIcon" runat="server" class="fas fa-shopping-bag"></i></h3>
            <asp:Repeater ID="rptrCartProducts" runat="server">
                <ItemTemplate>
                    <div class="media" style="border: 1px solid #eaeaec;">
                        <div class="media-left">
                            <a href="ProductView.aspx?PID=<%#Eval("ProdID") %>" target="_blank">
                                <img width="120px" class="media-object" src="Images/ProductImages/ProdID-<%#Eval("ProdID") %>/<%#Eval("Name") %><%#Eval("Extention") %>" alt="<%#Eval("Name") %>" onerror="this.src='Images/ProductImages/NoImage.jpg'">
                            </a>
                        </div>
                        <div class="media-body">
                            <h5 class="proNameCart"><%#Eval("ProdName") %></h5>
                            <p class="proPriceCart">Size: <%#Eval("SizeNamee") %></p>
                            <span class="proPrice">$<%# string.Format("{0:n}", Convert.ToInt64(Eval("ProdPrice"))-Convert.ToInt64(Eval("ProdDiscount"))) %></span>                            
                            <span class="priceGreen" style="font-size: 12px;" runat="server">($<%#Eval("ProdDiscount","{0:n}") %> OFF)</span>
                            <p>
                                <asp:Button CommandArgument='<%#Eval("ProdID")+"-"+ Eval("SizeIDD")%>' ID="btnRemoveItem" OnClick="btnRemoveItem_Click" CssClass="removeButton" runat="server" Text="Remove" />
                            </p>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="col-md-3" runat="server" id="divPriceDetails">
            <div style="border-bottom: 1px solid #eaeaec;">
                <h3>Price Details</h3>
                <div>
                    <label>Cart Total</label>
                    <span class="pull-right priceGray" id="spanCartTotal" runat="server"></span>
                </div>
                <div>
                    <label>Cart Discount</label>
                    <span class="pull-right priceGreen" id="spanDiscount" runat="server"></span>
                </div>
            </div>
            <div>
                <div class="proPriceView">
                    <label>Total</label>
                    <span class="pull-right" id="spanTotal" runat="server"></span>
                </div>
                <div>
                    <asp:Button ID="btnBuyNow" OnClick="btnBuyNow_Click" CssClass="buyNowButton" runat="server" Text="Buy Now" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

