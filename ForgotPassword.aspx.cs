﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Net.Mail;
using System.Net;

public partial class ForgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btPassRec_Click(object sender, EventArgs e)
    {
        String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from Customers where Email='" + tbEmail.Text + "'", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows.Count != 0)
            {
                string myGUID = Guid.NewGuid().ToString();
                string Email = Convert.ToString(dt.Rows[0][0]);
                SqlCommand cmd1 = new SqlCommand("insert into ForgotPassRequests values('" +myGUID+ "','"+Email+"',getdate())",con);
                cmd1.ExecuteNonQuery();
                lbPassRec.Text = "A password has been sent,  Please check your email.";
                lbPassRec.ForeColor = Color.Green;

                //generating email link
                String ToEmailAddress = dt.Rows[0][0].ToString();
                String fname = dt.Rows[0][1].ToString();
                String EmailBody = "Hello " + fname + ", <br/><br/> Click the link below to reset your password <br/> http://localhost:58487/RecoverPassword.aspx?Uid=" + myGUID;
                MailMessage PassRecMail = new MailMessage("BoardSeek@gmail.com",ToEmailAddress);
                PassRecMail.Body = EmailBody;
                PassRecMail.IsBodyHtml = true;
                PassRecMail.Subject = "BoardSeek Password recovery";

                SmtpClient SMTP = new SmtpClient("smtp.gmail.com", 587);
                SMTP.Credentials = new NetworkCredential()
                {
                    UserName = "BoardSeek@gmail.com",
                    Password = "benval123"
                };
                SMTP.EnableSsl = true;
                SMTP.Send(PassRecMail);

            }
            else
            {
                lbPassRec.Text = "Oops, the email you entered is not registered.";
                lbPassRec.ForeColor = Color.Red;
            }
            
        }
    }
}