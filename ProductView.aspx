﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="ProductView.aspx.cs" Inherits="ProductView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="padding-top: 50px">
        <div class="col-md-5">
            <%--Item Image--%>
            <div style="max-width: 350px" class="thumbnail">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <asp:Repeater ID="rptrImages" runat="server">
                            <ItemTemplate>
                                <div class="item <%# GetActiveClass (Container.ItemIndex)%>">
                                    <img src="Images/ProductImages/ProdID-<%#Eval("ProdID") %>/<%#Eval("Name") %><%#Eval("Extention") %>" alt="<%#Eval("Name") %>" onerror="this.src='Images/ProductImages/NoImage.jpg'" />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <%--Item Description--%>
            <asp:Repeater ID="rptrProductDetails" OnItemDataBound="rptrProductDetails_ItemDataBound" runat="server">
                <ItemTemplate>
                    <div class="divDet1">
                        <h1 class="proNameView"><%#Eval("ProdName") %></h1>
                        <h2 class="proBrandView"><%#Eval("BrandName") %></h2>
                        <span class="proOgPriceView">$<%#Eval("ProdPrice","{0:n}") %></span><span class="proPriceDiscountView">  $<%#Eval("ProdDiscount","{0:n}") %> OFF</span>
                        <p class="proPriceView">$<%# string.Format("{0:n}", Convert.ToInt64(Eval("ProdPrice"))-Convert.ToInt64(Eval("ProdDiscount"))) %></p>
                    </div>
                    <div>
                        <%--Size Select--%>
                        <h4 class="h5Size">SIZE</h4>
                        <div>
                            <asp:RadioButtonList ID="rblSize" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="divDet1">
                        <asp:Button ID="btnAddToCart" OnClick="btnAddToCart_Click" CssClass="mainButton" runat="server" Text="Add To Cart" />
                        <asp:Label ID="lblError" Visible="true" runat="server" CssClass="text-denger"></asp:Label>
                    </div>
                    <div class="divDet1">
                        <h5 class="h5Size">Product Description</h5>
                        <p>
                            <%#Eval("ProdDescription") %>
                        </p>
                        <h5 class="h5Size">Product Details</h5>
                        <p>
                            <%#Eval("ProdPDetails") %>
                        </p>
                        <h5 class="h5Size">Material Care</h5>
                        <p>
                            <%#Eval("ProdMaterialCare") %>
                        </p>
                    </div>
                    <div>
                        <p><%# ((int)Eval("FreeDelivery")==1) ? "Free Delivery":""%></p>
                        <p><%# ((int)Eval("30DayRet")==1) ? "30 Days Return":""%></p>
                        <p><%# ((int)Eval("COD")==1) ? "Cash on delivery":""%></p>
                    </div>

                    <asp:HiddenField ID="hfCatID" Value='<%#Eval("ProdCatID") %>' runat="server" />
                    <asp:HiddenField ID="hfSubCatID" Value='<%#Eval("ProdSubCatID") %>' runat="server" />
                    <asp:HiddenField ID="hfGenderID" Value='<%#Eval("ProdGenderID") %>' runat="server" />
                    <asp:HiddenField ID="hfBrandID" Value='<%#Eval("ProdBrandID") %>' runat="server" />
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>

