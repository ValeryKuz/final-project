﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminHome : System.Web.UI.Page
{
    public static String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        ProdCount();
        ClientsCount();
        OrdersCount();
        SurfCount();
        BindPurchase();
    }

    private void SurfCount()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            using (SqlCommand cmd = new SqlCommand("select COUNT(SurfboardID)from SurfBoards", con))
            {
                con.Open();
                Int32 countBoards = (Int32)cmd.ExecuteScalar();
                txtBoards.InnerText = countBoards.ToString();
            }
        }
    }

    private void OrdersCount()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            using (SqlCommand cmd = new SqlCommand("select COUNT(PurchaseID)from Purchase", con))
            {
                con.Open();
                Int32 countOrders = (Int32)cmd.ExecuteScalar();
                txtOrders.InnerText = countOrders.ToString();
            }
        }
    }

    private void ProdCount()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            using (SqlCommand cmd = new SqlCommand("select COUNT(ProdID)from Products", con))
            {
                con.Open();
                Int32 countProd = (Int32)cmd.ExecuteScalar();
                txtProd.InnerText = countProd.ToString();
            }
        }
        
    }

    private void ClientsCount()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            using (SqlCommand cmd = new SqlCommand("SELECT Count(CustID) FROM Customers where UserType='U'", con))
            {
                con.Open();
                Int32 countClients = (Int32)cmd.ExecuteScalar();
                txtUsers.InnerText = countClients.ToString();
            }
        }

    }

    private void BindPurchase()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            using (SqlCommand cmd = new SqlCommand("select * from Purchase", con))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                {
                    DataTable dtBrands = new DataTable();
                    sda.Fill(dtBrands);
                    rptrCategory.DataSource = dtBrands;
                    rptrCategory.DataBind();
                }
            }
        }
    }
}