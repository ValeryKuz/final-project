﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void BtAdminLogout_Click(object sender, EventArgs e) //When the Admin logsout we will redirect to the Deafult page
    {
        Session["EMAIL"] = null;
        Response.Redirect("~/Default.aspx");
    }
}
