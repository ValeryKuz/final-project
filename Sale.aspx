﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="Sale.aspx.cs" Inherits="Sale" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row" style="padding-top: 50px">
        <!--Repeater to show all Products-->
        <asp:Repeater ID="rptrProducts" runat="server">
            <ItemTemplate>
                <div class="col-sm-3 col-md-3">
                    <a style="text-decoration: none;" href="ProductView.aspx?PID=<%#Eval("ProdID") %>">
                        <div class="thumbnail">
                            <img src="Images/ProductImages/ProdID-<%#Eval("ProdID") %>/<%#Eval("ImageName") %><%#Eval("Extention") %>" alt="<%#Eval("ImageName") %>" />
                            <div class="caption">
                                <div class="proBrand"><%#Eval("BrandName") %></div>
                                <div class="proName prodNameDesc"><%#Eval("ProdName") %></div>
                                <div class="proPrice"><span class="proOgPrice">$<%#Eval("ProdPrice","{0:n}") %></span> <span class="proPriceDiscount">$<%#Eval("DiscAmount","{0:n}") %> OFF </span>
                                    <br />
                                    $<%#Eval("ProdSellPrice","{0:n}") %> </div>
                            </div>
                        </div>
                    </a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>

