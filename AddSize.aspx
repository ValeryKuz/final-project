﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="AddSize.aspx.cs" Inherits="AddSize" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderAdmin" Runat="Server">
         <div class="container">
            <div class="form-horizontal">
                <h2>Add Size</h2>
                <hr />
               <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="col-md-2 control-label" Text="Size Name"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtSize" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorSize" CssClass="text-danger" runat="server" ErrorMessage="Please enter Size Name" ControlToValidate="txtSize"></asp:RequiredFieldValidator>
                    </div>
                </div>
              <div class="form-group">
                <asp:Label ID="Label2" runat="server" CssClass="col-md-2 control-label" Text="Brand"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlBrands" CssClass="form-control" runat="server"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorBrands" CssClass="text-danger" runat="server" ErrorMessage="Please select Brand" ControlToValidate="ddlBrands" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label5" runat="server" CssClass="col-md-2 control-label" Text="Main Category"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlCategory" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorCategory" CssClass="text-danger" runat="server" ErrorMessage="Please select Category" ControlToValidate="ddlCategory" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label3" runat="server" CssClass="col-md-2 control-label" Text="Sub Category"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlSubCat" CssClass="form-control" runat="server"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorSubCat" CssClass="text-danger" runat="server" ErrorMessage="Please select Sub Category" ControlToValidate="ddlSubCat" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label1" runat="server" CssClass="col-md-2 control-label" Text="Gender"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlGender" CssClass="form-control" runat="server"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorGender" CssClass="text-danger" runat="server" ErrorMessage="Please select Gender" ControlToValidate="ddlGender" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-success" OnClick="btnAdd_Click" />
                    </div>
                </div>
            </div>
              <h1>Add Sizes</h1>
            <hr />
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">All Brands</div>

                <!-- Adding repeater to insert all data from Brands table -->
                <asp:Repeater ID="rptrCategory" runat="server">  
                    <HeaderTemplate>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Brand</th>
                                    <th>Category</th>
                                    <th>SubCategory</th>
                                    <th>Gender</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate> <!-- Repeating items -->
                        <tr>
                            <th><%# Eval ("SizeID")%></th>
                            <td><%# Eval ("SizeName") %></td>
                            <td><%# Eval ("Name") %></td>
                            <td><%# Eval ("CatName") %></td>
                            <td><%# Eval ("SubCatName") %></td>
                            <td><%# Eval ("GenderName") %></td>

                            <td>Edit</td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>

            </div>
        </div>
</asp:Content>

