﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignIn.aspx.cs" Inherits="SignIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sign In</title>
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/dropdown.css" rel="stylesheet" />
    <link href="css/SignIn.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">
        <!-- Top start -->
        <div>
            <nav class="navbar-inverse navbar-static-top" role="navigation" id="TopNavigation">
                <div class="container-fluid">
                    <ul class="nav navbar-nav" id="topNav1">
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Delivery Info</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" id="topNav2">
                        <li><a href="SignUp.aspx"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li class="active"><a href="SignIn.aspx"><span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
                    </ul>
                </div>
            </nav>

            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <!-- This button will be desplayed when the website is loaded from small devices -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <!-- This adds 3 lines to our toggle button -->
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="nav-brand" href="Default.aspx"><span>
                            <img alt="Logo" src="images/logo.png" height="50" /></span></a>
                    </div>
                    <div class="collapse navbar-collapse js-navbar-collapse">
                        <ul class="nav navbar-nav navbar">
                            <li><a href="BoardMatching.aspx">Board Matcher</a></li>
                            <li class="dropdown dropdown-large">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Accessories<b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-menu-large navbar-nav" id="dropDownMenu">
                                    <li class="col-sm-6">
                                        <ul>
                                            <li class="dropdown-header">Fins</li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                        </ul>
                                    </li>
                                    <li class="col-sm-6">
                                        <ul>
                                            <li class="dropdown-header">Bags</li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                        </ul>
                                    </li>
                                </ul>

                            </li>
                            <li><a href="Products.aspx">Products</a></li>
                            <li><a href="#">What's new</a></li>
                            <li><a href="#">Back in stock</a></li>
                            <li><a href="#">Style deals</a></li>
                            <li><a href="Sale.aspx" id="SaleFont">Sale</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top end -->
        <!-- Sign In start -->
        <div class="container">
            <!-- Form start -->
            <div class="form-horizontal col-sm-6">
                <h3>Sign in</h3>
                <h4>with your BoardSeek account </h4>
                <asp:Label ID="lblError" runat="server" CssClass="text-danger"></asp:Label>
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="col-md-2 control-label" Text="Email"></asp:Label>
                    <div class="col-md-6">
                        <asp:TextBox ID="Email" CssClass="form-control" runat="server" TextMode="Email"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" CssClass="text-danger" runat="server" ErrorMessage="Please enter Username" ControlToValidate="Email"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="col-md-2 control-label" Text="Password"></asp:Label>
                    <div class="col-md-6">
                        <asp:TextBox ID="Password" TextMode="Password" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" CssClass="text-danger" runat="server" ErrorMessage="Please enter Password" ControlToValidate="Password"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <!-- Helps to place correctly the checkbox -->
                    <div class="col-md-6">
                        <asp:CheckBox ID="CheckBox1" runat="server" />
                        <asp:Label ID="Label3" runat="server" CssClass="control-label" Text="Remember Me?"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <asp:Button ID="BtLogIn" runat="server" Text="LogIn" CssClass="btn btn-success" OnClick="ButLogIn_Click" />
                        <asp:LinkButton ID="LinkButtonSignIn" runat="server" PostBackUrl="~/SignUp.aspx" CausesValidation="False">Sign Up</asp:LinkButton>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <asp:LinkButton ID="lbForgotPW" runat="server" PostBackUrl="~/ForgotPassword.aspx" CausesValidation="False">Forgot Password?</asp:LinkButton>
                    </div>
                </div>
            </div>
            <!-- Form end -->
            <div class="row col-sm-6" style="float: right;">
                <div class="thumbnail thumbnailboardImg text-center">
                    <img src="Images/signIn.jpg" class="img-responsive"/>
                    <div class="captionboardImg">
                        <p>Sign in - in order to safeguard the purchases in our website.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sign In end -->
    </form>
    <!-- Footer start -->
    <hr />
    <footer>
        <div class="container-fluid bg-faded mt-5">
            <div class="container">
                <div class="row">
                    <!-- footer column 1 start -->
                    <div class="col-md-2 col-md-offset-1">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>CUSTOMER CARE</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="#">Help Center</a></li>
                                    <li class="fList"><a href="#">FAQ</a></li>
                                    <li class="fList"><a href="deliveryinformation.html">Delivery</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 1 end -->
                    <!-- footer column 2 start -->
                    <div class="col-md-2">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>ABOUT US</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="/OurStories.aspx">Our Stories</a></li>
                                    <li class="fList"><a href="/Press.aspx">Press</a></li>
                                    <li class="fList"><a href="/Career.aspx">Career</a></li>
                                    <li class="fList"><a href="/ContactUs.aspx">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 2 end -->
                    <!-- footer column 3 start -->
                    <div class="col-md-2">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>MY ACCOUNT</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="SignUp.aspx">Register</a></li>
                                    <li class="fList"><a href="#">My Cart</a></li>
                                    <li class="fList"><a href="#">Order History</a></li>
                                    <li class="fList"><a href="buy.html">Payment</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 3 end -->
                    <!-- footer column 4 start -->
                    <div class="col-md-4">
                        <!-- row start -->
                        <div class="row py-2 ">
                            <div>
                                <h4>OUR MAIN STORES</h4>
                                <button type="button" onclick="changeCityIsrael()" class="btn btn-default">Israel</button>
                                <button type="button" onclick="changeCityAustralia()" class="btn btn-default">Australia</button>
                                <ul class="list-group">
                                    <li class="fList"><i class="fa fa-map-marker" aria-hidden="true"></i><span id="adress"> 2990 Haifa , Israel</span></li>
                                    <li class="fList"><i class="fa fa-phone" aria-hidden="true"></i><span id="phone"> 025-2839341</span></li>
                                    <li class="fList"><a href="info@BoardSeek.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@boardseek.com</a></li>
                                </ul>
                            </div>
                            <!-- row end -->
                        </div>
                        <!-- footer column 4 end -->
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="container-fluid bg-white py-3">
            <div class="container">
                <div class="container-">
                    <p class="pull-right">
                        <a href="#">
                            <img src="images/arrow-up.png" /></a>
                    </p>
                    <p class="text-muted">&copy; 2017 BoardSeek.com &middot; <a href="Default.aspx" class="text-muted">Home</a> &middot; <a href="#" class="text-muted">About</a> &middot; <a href="#" class="text-muted">Contact</a> &middot;</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/MenuHover.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
