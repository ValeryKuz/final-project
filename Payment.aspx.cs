﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Payment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EMAIL"] != null)
        {
            BindPriceData();
        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }
    }

    private void BindPriceData()
    {
        if (Request.Cookies["CartPID"] != null)
        {
            string CookieData = Request.Cookies["CartPID"].Value.Split('=')[1];
            string[] CookieDataArray = CookieData.Split(',');

            if (CookieDataArray.Length > 0)
            {
                DataTable dtBrands = new DataTable();
                Int64 CartTotal = 0;
                Int64 Total = 0;
                Int64 DiscountTotal = 0;

                for (int i = 0; i < CookieDataArray.Length; i++) //Separate each Product+SIze
                {
                    string PID = CookieDataArray[i].ToString().Split('-')[0];
                    string SizeID = CookieDataArray[i].ToString().Split('-')[1];

                    if (hdPidSizeID.Value != null && hdPidSizeID.Value != "")
                    {
                        hdPidSizeID.Value += "," + PID + "-" + SizeID;
                    }
                    else
                    {
                        hdPidSizeID.Value = PID + "-" + SizeID;
                    }

                    String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(CS))
                    {
                        using (SqlCommand cmd = new SqlCommand("select A.*,dbo.getSizeName(" + SizeID + ") as SizeNamee,"
                            + SizeID + " as SizeIDD,SizeData.Name,SizeData.Extention from Products A cross apply(select top 1 B.Name, Extention from ProductImages B where B.ProdID = A.ProdID) SizeData where A.ProdID="
                            + PID + "", con)) //Passing the Product ID to get the product details from DB by using function
                        {
                            cmd.CommandType = CommandType.Text;
                            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                            {
                                sda.Fill(dtBrands);
                            }
                        }
                    }
                    CartTotal += Convert.ToInt64(dtBrands.Rows[i]["ProdPrice"]);
                    if ((dtBrands.Rows[i]["ProdDiscount"]) != DBNull.Value)
                    {
                        DiscountTotal += Convert.ToInt64(dtBrands.Rows[i]["ProdDiscount"]);
                    }
                    else
                    {
                        DiscountTotal += 0;
                    }
                }
                divPriceDetails.Visible = true;
                Total = CartTotal - DiscountTotal;

                spanCartTotal.InnerText =CartTotal.ToString();
                spanTotal.InnerText = Total.ToString();
                spanDiscount.InnerText = (CartTotal - Total).ToString();

                hdCartAmount.Value = CartTotal.ToString();
                hdCartDiscount.Value = (CartTotal - Total).ToString();
                hdTotalPaid.Value = Total.ToString();
            }

            else
            {
                Response.Redirect("~/Products.aspx");
            }

        }

        else
        {
            Response.Redirect("~/Products.aspx");
        }
    }

    protected void btnVisa_Click(object sender, EventArgs e)
    {
        if (Session["CUSTID"] != null)
        {
            DateTime dateValidator = new DateTime();
            if (!DateTime.TryParse(txtValidationDate.Text, out dateValidator))
            {
                lblMsgDate.ForeColor = Color.Red;
                lblMsgDate.Text = "Enter a valid date by the format.";
            }
            else
            {

                string USERID = Session["CUSTID"].ToString();
                string PaymentType = "Visa";
                string PaymentStatus = "Paid";
                DateTime DateOfPurchase = DateTime.Now;

                //Insert data to table Purchase
                String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("insert into Purchase values('" + USERID + "','"
                        + hdPidSizeID.Value + "','" + hdCartAmount.Value + "','" + hdCartDiscount.Value + "','"
                        + hdTotalPaid.Value + "','" + PaymentType + "','" + PaymentStatus + "','" + DateOfPurchase + "','"
                        + txtName.Text + "','" + txtAddress.Text + "','" + txtPinCode.Text + "','" + txtPhone.Text+"') select SCOPE_IDENTITY()", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    if (Request.Cookies["CartPID"] != null) //Check if Cookies exist, if not create one
                    {
                        Response.Cookies["CartPID"].Expires = DateTime.Now.AddDays(-1);
                        Response.Redirect("/SuccessfulPurchase.aspx");
                    }
                    else
                    {
                        Response.Redirect("/SuccessfulPurchase.aspx");
                    }
                }

            }
        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }
    }
}