﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="AddSurfBoard.aspx.cs" Inherits="AddSurfBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderAdmin" Runat="Server">
      <div class="container">
        <div class="form-horizontal">
            <h2>Add Surfboard</h2>
            <hr />

             <div class="form-group">
                <asp:Label ID="lblCat" runat="server" CssClass="col-md-2 control-label" Text="Category"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlCategory" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                           <asp:ListItem Selected="True" Value="7"></asp:ListItem> 
                       </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorCategory" CssClass="text-danger" runat="server" ErrorMessage="Please select Category" ControlToValidate="ddlCategory" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="lblSubCat" runat="server" CssClass="col-md-2 control-label" Text="Sub Category"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlSubCategory" OnSelectedIndexChanged="ddlSubCat_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                       </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorSubCategory" CssClass="text-danger" runat="server" ErrorMessage="Please select Sub Category" ControlToValidate="ddlSubCategory" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="lblGender" runat="server" CssClass="col-md-2 control-label" Text="Gender"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlGender" OnSelectedIndexChanged="ddlGender_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                           <asp:ListItem Selected="True" Value="1"></asp:ListItem> 
                       </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorGender" CssClass="text-danger" runat="server" ErrorMessage="Please select Gender" ControlToValidate="ddlGender" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
             <div class="form-group hidden">
                <asp:Label ID="lblSize" runat="server" CssClass="col-md-2 control-label" Text="Size"></asp:Label>
                   <div class="col-md-3">
                       <asp:CheckBoxList ID="cblSize" CssClass="form-control" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                   </div>
              </div>
            <div class="form-group">
                <asp:Label ID="Label19" runat="server" CssClass="col-md-2 control-label" Text="Quantity"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="txtQuantity" CssClass="form-control" runat="server" Text="1"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" runat="server" ErrorMessage="Please enter Quantity" ControlToValidate="txtQuantity"></asp:RequiredFieldValidator>
                   </div>
              </div>


              <div class="form-group">
                <asp:Label ID="Label1" runat="server" CssClass="col-md-2 control-label" Text="Model"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="txtPName" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorPName" CssClass="text-danger" runat="server" ErrorMessage="Please enter Product Name" ControlToValidate="txtPName"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label2" runat="server" CssClass="col-md-2 control-label" Text="Surfboard Price"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="txtPPrice" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorPPrice" CssClass="text-danger" runat="server" ErrorMessage="Please enter Price" ControlToValidate="txtPPrice"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label3" runat="server" CssClass="col-md-2 control-label" Text="Sale Price (Optional)"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="txtSellPrice" CssClass="form-control" runat="server"></asp:TextBox>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label4" runat="server" CssClass="col-md-2 control-label" Text="Brand"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlBrands" CssClass="form-control" runat="server"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorBrand" CssClass="text-danger" runat="server" ErrorMessage="Please select Brand" ControlToValidate="ddlBrands" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
             <div class="form-group">
                <asp:Label ID="Label5" runat="server" CssClass="col-md-2 control-label" Text="Volume"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="tbVolume" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="text-danger" runat="server" ErrorMessage="Please enter Price" ControlToValidate="txtPPrice"></asp:RequiredFieldValidator>
                   </div>
              </div>
             <div class="form-group">
                <asp:Label ID="Label6" runat="server" CssClass="col-md-2 control-label" Text="Height"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="tbHeight" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="text-danger" runat="server" ErrorMessage="Please enter Price" ControlToValidate="txtPPrice"></asp:RequiredFieldValidator>
                   </div>
              </div>
             <div class="form-group">
                <asp:Label ID="Label7" runat="server" CssClass="col-md-2 control-label" Text="Width"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="tbWidth" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator4" CssClass="text-danger" runat="server" ErrorMessage="Please enter Price" ControlToValidate="txtPPrice"></asp:RequiredFieldValidator>
                   </div>
              </div>
                          <div class="form-group">
                <asp:Label ID="Label14" runat="server" CssClass="col-md-2 control-label" Text="Wave size suitable:"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlwSize" CssClass="form-control" runat="server">
                        <asp:ListItem Value="1">Knee to waist high</asp:ListItem>
                        <asp:ListItem Value="2">Waist to chest high</asp:ListItem>
                        <asp:ListItem Value="3">Chest to head high</asp:ListItem>
                        <asp:ListItem Value="4">Overhead</asp:ListItem>
                        <asp:ListItem Value="5">Double overhead plus</asp:ListItem>
                       </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="text-danger" runat="server" ErrorMessage="Please select Brand" ControlToValidate="ddlBrands" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
                 <div class="form-group">
                <asp:Label ID="Label21" runat="server" CssClass="col-md-2 control-label" Text="Wave type suitable:"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlwType" CssClass="form-control" runat="server">
                        <asp:ListItem Value="1">Soft and mushy</asp:ListItem>
                        <asp:ListItem Value="2">Rolling</asp:ListItem>
                        <asp:ListItem Value="3">Pitching and fast breaking</asp:ListItem>
                        <asp:ListItem Value="4">Hollow and barrelish</asp:ListItem>
                       </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="text-danger" runat="server" ErrorMessage="Please select Brand" ControlToValidate="ddlBrands" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
                          <div class="form-group">
                <asp:Label ID="Label22" runat="server" CssClass="col-md-2 control-label" Text="Skill requested:"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlSkill" CssClass="form-control" runat="server">
                        <asp:ListItem Value="1">Wishing to start</asp:ListItem>
                        <asp:ListItem Value="2">Beginner</asp:ListItem>
                        <asp:ListItem Value="3">Intermediate</asp:ListItem>
                        <asp:ListItem Value="4">Advanced</asp:ListItem>
                        <asp:ListItem Value="4">Sponsored</asp:ListItem>
                       </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator7" CssClass="text-danger" runat="server" ErrorMessage="Please select Brand" ControlToValidate="ddlBrands" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
                          <div class="form-group">
                <asp:Label ID="Label23" runat="server" CssClass="col-md-2 control-label" Text="Tricks suitable:"></asp:Label>
                   <div class="col-md-3">
                       <asp:DropDownList ID="ddlTricks" CssClass="form-control" runat="server">
                            <asp:ListItem Value="1">Learn to surf</asp:ListItem>
                        <asp:ListItem Value="2">Catch more Waves</asp:ListItem>
                        <asp:ListItem Value="3">Nail more tricks</asp:ListItem>
                        <asp:ListItem Value="4">Try something CRAZY!</asp:ListItem>
                       </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator8" CssClass="text-danger" runat="server" ErrorMessage="Please select Brand" ControlToValidate="ddlBrands" InitialValue="0"></asp:RequiredFieldValidator>
                   </div>
              </div>
            <div class="form-group"></div>
             <label class="col-xs-11">What is your surfing style?</label>
                <div class="col-xs-11">
                    <asp:CheckBox ID="CB1" runat="server" Text="Simple Cruising" /><br />
                    <asp:CheckBox ID="CB2" runat="server" Text="On the Nose" />
                    <br />
                    <asp:CheckBox ID="CB3" runat="server" Text="Wide Carves" />
                    <br />
                    <asp:CheckBox ID="CB4" runat="server" Text="Fast Cuts & Snaps" />
                    <br />
                    <asp:CheckBox ID="CB5" runat="server" Text="Big Airs" />
                    <br />
                    <asp:CheckBox ID="CB6" runat="server" Text="Getting Barreled" />
                </div>           
              <div class="form-group">
                <asp:Label ID="Label8" runat="server" CssClass="col-md-2 control-label" Text="Descriptions"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="txtDesc" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorDesc" CssClass="text-danger" runat="server" ErrorMessage="Please fill Description" ControlToValidate="txtDesc"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label9" runat="server" CssClass="col-md-2 control-label" Text="Product Details"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="txtPDetails" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorPDetails" CssClass="text-danger" runat="server" ErrorMessage="Please fill Details" ControlToValidate="txtPDetails"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label15" runat="server" CssClass="col-md-2 control-label" Text="Material Care"></asp:Label>
                   <div class="col-md-3">
                      <asp:TextBox ID="txtPMaterialCare" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidatorPMC" CssClass="text-danger" runat="server" ErrorMessage="Please fill Details" ControlToValidate="txtPMaterialCare"></asp:RequiredFieldValidator>
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label10" runat="server" CssClass="col-md-2 control-label" Text="Upload Image"></asp:Label>
                   <div class="col-md-3">
                       <asp:FileUpload ID="fuImg01" CssClass="form-control" runat="server" />
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label11" runat="server" CssClass="col-md-2 control-label" Text="Upload Image"></asp:Label>
                   <div class="col-md-3">
                       <asp:FileUpload ID="fuImg02" CssClass="form-control" runat="server" />
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label12" runat="server" CssClass="col-md-2 control-label" Text="Upload Image"></asp:Label>
                   <div class="col-md-3">
                       <asp:FileUpload ID="fuImg03" CssClass="form-control" runat="server" />
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label13" runat="server" CssClass="col-md-2 control-label" Text="Upload Image"></asp:Label>
                   <div class="col-md-3">
                       <asp:FileUpload ID="fuImg04" CssClass="form-control" runat="server" />
                   </div>
              </div>
              <div class="form-group">
                <asp:Label ID="Label20" runat="server" CssClass="col-md-2 control-label" Text="Upload Image"></asp:Label>
                   <div class="col-md-3">
                       <asp:FileUpload ID="fuImg05" CssClass="form-control" runat="server" />
                   </div>
              </div>
            <div class="form-group">
                <asp:Label ID="Label16" runat="server" CssClass="col-md-2 control-label" Text="Free Delivery"></asp:Label>
                <div class="col-md-3">
                    <asp:CheckBox ID="cbFD" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <asp:Label ID="Label17" runat="server" CssClass="col-md-2 control-label" Text="30 Days Return"></asp:Label>
                <div class="col-md-3">
                    <asp:CheckBox ID="cb30Ret" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <asp:Label ID="Label18" runat="server" CssClass="col-md-2 control-label" Text="COD"></asp:Label> <!--Cash on delivery -->
                <div class="col-md-3">
                    <asp:CheckBox ID="cbCOD" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-success" OnClick="btnAdd_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

