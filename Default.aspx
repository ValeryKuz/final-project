﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BoardSeek</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/dropdown.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">
        <!-- Top start -->
        <div>
            <nav class="navbar-inverse navbar-static-top" role="navigation" id="TopNavigation">
                <div class="container-fluid">
                    <ul class="nav navbar-nav" id="topNav1">
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Delivery Info</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" id="topNav2">
                        <li><a href="#" id="btnManage" runat="server"><span class="glyphicon glyphicon-user"></span> Manage</a></li>
                        <li>
                            <asp:LinkButton ID="btnCart" runat="server" CssClass="btn btn-info" OnClick="btnCart_Click"><span class="glyphicon glyphicon-shopping-cart"></span><span id="pCount" runat="server"></span></asp:LinkButton>
                        </li>
                        <li id="btnSignUp" runat="server"><a href="SignUp.aspx"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li id="btnSignIn" runat="server"><a href="SignIn.aspx"><span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
                        <li>
                            <asp:LinkButton ID="btnSignOut" runat="server" CssClass="btn btn-warning" OnClick="btnSignOut_Click"><span aria-hidden="true" class="glyphicon glyphicon-log-out"></span> Log Out</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <!-- This button will be desplayed when the website is loaded from small devices -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <!-- This adds 3 lines to our toggle button -->
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="nav-brand" href="Default.aspx"><span>
                            <img alt="Logo" src="images/logo.png" height="50" /></span></a>
                    </div>
                    <div class="collapse navbar-collapse js-navbar-collapse">
                        <ul class="nav navbar-nav navbar">
                            <li><a href="BoardMatching.aspx">Board Matcher</a></li>
                            <li class="dropdown dropdown-large">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Accessories<b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-menu-large navbar-nav" id="dropDownMenu">
                                    <li class="col-sm-6">
                                        <ul>
                                            <li class="dropdown-header">Sword of Truth</li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                        </ul>
                                    </li>
                                    <li class="col-sm-6">
                                        <ul>
                                            <li class="dropdown-header">Panda</li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                        </ul>
                                    </li>
                                </ul>

                            </li>
                            <li><a href="Products.aspx">Products</a></li>
                            <li><a href="#WhatsNew">What's new</a></li>
                            <li><a href="#">Back in stock</a></li>
                            <li><a href="#">Style deals</a></li>
                            <li><a href="Sale.aspx" id="SaleFont">Sale</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Navbar ends -->
            <!-- Images carousel -->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="Images/Carousel/0.jpg" alt="Supportive" style="width: 100%;" />
                        <div class="carousel-caption">
                            <h3 style= "color:black">Supportive!</h3>
                            <p style= "color:black"><b>Supporting WSL events all over the world among juniors!</b></p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="Images/Carousel/1.jpg" alt="Chicago" style="width: 100%;" />
                        <div class="carousel-caption">
                            <h3 style= "color:black">Wave Tracker</h3>
                            <p style= "color:black">New upcoming wave tracking watch! stay tuned...</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="Images/Carousel/2.jpg" alt="New York" style="width: 100%;" />
                        <div class="carousel-caption">
                            <h3>Women apparel</h3>
                            <p>Check our brand new women apparel</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="Images/Carousel/3.jpg" alt="Chicago" style="width: 100%;" />
                        <div class="carousel-caption">
                            <h3 style= "color:black">RVCA new products in our store now!</h3>
                            <p style= "color:black">Check them out!</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="Images/Carousel/4.jpg" alt="Roxy" style="width: 100%;" />
                        <div class="carousel-caption">
                            <h3 style= "color:black">New movie</h3>
                            <p style= "color:black">A new movie from John John will be up for viewing through our website soon!</p>
                        </div>
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- Images carousel end -->
        </div>
        <!-- Top end -->
        <!-- Middle content start -->
        <a id ="WhatsNew"></a>
        <h1 style="padding-top: 60px;" class="text-center">Brand new:</h1>
<div class="container">
    <div class="row">
        <div class="col-xs-3">
            <a href="/ProductView.aspx?PID=24" class="thumbnail">
                <img src="Images/ProductImages/ProdID-24/Lovebird-01.jpg"  alt="125x125"/>
                    <p>Lovebird model - by Bing surfboards</p>
                    <p> Price: 5400$</p>
            </a>
        </div>
        <div class="col-xs-3">
            <a href="/ProductView.aspx?PID=19" class="thumbnail">
                <img src="Images/ProductImages/ProdID-19/Porter%20Top-01.jpg"  alt="125x125" />
                <p>O'neill Porter top Summer Flowers</p>
                <p>Price: 42$</p>
            </a>
        </div>
        <div class="col-xs-3">
            <a href="/ProductView.aspx?PID=30" class="thumbnail">
               <img src="Images/ProductImages/ProdID-30/FCS%20II%20MF%20signature-01.jpeg"  alt="125x125" />
                <p>Mick Fanning signature Thruster set</p>
                <p>Price: 390$</p>
            </a>
        </div>
        <div class="col-xs-3">
            <a href="/ProductView.aspx?PID=31" class="thumbnail">
                <img src="Images/ProductImages/ProdID-31/Future%20-%20Dane%20raynolds-01.jpg"  alt="125x125" />
                <p>Dane Raynolds signature Thruster set</p>
                <p>Price: 310$</p>
            </a>
        </div>
    </div>
</div>
        <!-- Middle content end -->
    </form>
    <!-- Footer start -->
    <hr />
    <footer>
        <div class="container-fluid bg-faded mt-5">
            <div class="container">
                <div class="row">
                    <!-- footer column 1 start -->
                    <div class="col-md-2 col-md-offset-1">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>CUSTOMER CARE</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="#">Help Center</a></li>
                                    <li class="fList"><a href="#">FAQ</a></li>
                                    <li class="fList"><a href="deliveryinformation.html">Delivery</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 1 end -->
                    <!-- footer column 2 start -->
                    <div class="col-md-2">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>ABOUT US</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="/OurStories.aspx">Our Stories</a></li>
                                    <li class="fList"><a href="/Press.aspx">Press</a></li>
                                    <li class="fList"><a href="/Career.aspx">Career</a></li>
                                    <li class="fList"><a href="/ContactUs.aspx">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 2 end -->
                    <!-- footer column 3 start -->
                    <div class="col-md-2">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>MY ACCOUNT</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="SignUp.aspx">Register</a></li>
                                    <li class="fList"><a href="#">My Cart</a></li>
                                    <li class="fList"><a href="#">Order History</a></li>
                                    <li class="fList"><a href="buy.html">Payment</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 3 end -->
                    <!-- footer column 4 start -->
                    <div class="col-md-4">
                        <!-- row start -->
                        <div class="row py-2 ">
                            <div>
                                <h4>OUR MAIN STORES</h4>
                                <button type="button" onclick="changeCityIsrael()" class="btn btn-default">Israel</button>
                                <button type="button" onclick="changeCityAustralia()" class="btn btn-default">Australia</button>
                                <ul class="list-group">
                                    <li class="fList"><i class="fa fa-map-marker" aria-hidden="true"></i><span id="adress"> 2990 Haifa , Israel</span></li>
                                    <li class="fList"><i class="fa fa-phone" aria-hidden="true"></i><span id="phone"> 025-2839341</span></li>
                                    <li class="fList"><a href="info@simplefashion.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@boardseek.com</a></li>
                                </ul>
                            </div>
                            <!-- row end -->
                        </div>
                        <!-- footer column 4 end -->
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="container-fluid bg-white py-3">
            <div class="container">
                <div class="container-">
                    <p class="pull-right">
                        <a href="#">
                            <img src="images/arrow-up.png" /></a>
                    </p>
                    <p class="text-muted">&copy; 2017 BoardSeek.com &middot; <a href="Default.aspx" class="text-muted">Home</a> &middot; <a href="#" class="text-muted">About</a> &middot; <a href="#" class="text-muted">Contact</a> &middot;</p>
                </div>
            </div>
        </div>

    </footer>
    <!-- Footer end -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/MenuHover.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
