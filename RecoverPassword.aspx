﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RecoverPassword.aspx.cs" Inherits="RecoverPassword" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Reset your password</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/dropdown.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">
        <!-- Top start -->
        <div>
            <nav class="navbar-inverse navbar-static-top" role="navigation" id="TopNavigation">
                <div class="container-fluid">
                    <ul class="nav navbar-nav" id="topNav1">
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Delivery Info</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" id="topNav2">
                        <li id="btnSignUp" runat="server"><a href="SignUp.aspx"><span class="glyphicon glyphicon-user"></span>Sign Up</a></li>
                        <li id="btnSignIn" runat="server"><a href="SignIn.aspx"><span class="glyphicon glyphicon-log-in"></span>Sign In</a></li>
                    </ul>
                </div>
            </nav>

            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <!-- This button will be desplayed when the website is loaded from small devices -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <!-- This adds 3 lines to our toggle button -->
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="nav-brand" href="Default.aspx"><span>
                            <img alt="Logo" src="images/logo.png" height="50" /></span></a>
                    </div>
                    <div class="collapse navbar-collapse js-navbar-collapse">
                        <ul class="nav navbar-nav navbar">
                            <li><a href="BoardMatching.aspx">Board Matcher</a></li>
                            <li class="dropdown dropdown-large">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Accessories<b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-menu-large navbar-nav" id="dropDownMenu">
                                    <li class="col-sm-6">
                                        <ul>
                                            <li class="dropdown-header">Sword of Truth</li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                        </ul>
                                    </li>
                                    <li class="col-sm-6">
                                        <ul>
                                            <li class="dropdown-header">Panda</li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                            <li><a href="#">Example</a></li>
                                        </ul>
                                    </li>
                                </ul>

                            </li>
                            <li><a href="Products.aspx">Products</a></li>
                            <li><a href="#">What's new</a></li>
                            <li><a href="#">Back in stock</a></li>
                            <li><a href="#">Style deals</a></li>
                            <li><a href="#" id="SaleFont">Sale</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Navbar ends -->

        </div>
        <!-- Top end -->
        <!-- Middle content start -->
        <div class="container">
            <div class="form-horizontal">
                <h2>Reset Password</h2>
                <hr />
                <div class="form-group">
                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="X-Large"></asp:Label>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblPassword" runat="server" CssClass="col-md-2 control-label" Text="Enter new password" Visible="False"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="tbNewPass" CssClass="form-control" runat="server" TextMode="Password" Visible="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" CssClass="text-danger" runat="server" ErrorMessage="Please enter your new password." ControlToValidate="tbNewPass" Visible="False"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblRetypePass" runat="server" CssClass="col-md-2 control-label" Text="Re-Type new password" Visible="False"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="tbRetypePass" CssClass="form-control" runat="server" TextMode="Password" Visible="False"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidatorPass" runat="server" CssClass="text-danger" ErrorMessage="The passwords you entered do not match." ControlToValidate="tbNewPass" ControlToCompare="tbRetypePass" Visible="False"></asp:CompareValidator>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <asp:Button ID="btRecPass" runat="server" CssClass="btn btn-default" Text="Reset" Visible="False" OnClick="btRecPass_Click" />
                    </div>
                </div>
            </div>
            <!-- Form-horizontal ends -->
        </div>
        <!-- Container ends -->
        <!-- Middle content end -->
    </form>
    <!-- Footer start -->
    <hr />
    <footer>
        <div class="container-fluid bg-faded mt-5">
            <div class="container">
                <div class="row">
                    <!-- footer column 1 start -->
                    <div class="col-md-2 col-md-offset-1">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>CUSTOMER CARE</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="#">Help Center</a></li>
                                    <li class="fList"><a href="#">FAQ</a></li>
                                    <li class="fList"><a href="deliveryinformation.html">Delivery</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 1 end -->
                    <!-- footer column 2 start -->
                    <div class="col-md-2">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>ABOUT US</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="/OurStories.aspx">Our Stories</a></li>
                                    <li class="fList"><a href="/Press.aspx">Press</a></li>
                                    <li class="fList"><a href="/Career.aspx">Career</a></li>
                                    <li class="fList"><a href="/ContactUs.aspx">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 2 end -->
                    <!-- footer column 3 start -->
                    <div class="col-md-2">
                        <!-- row start -->
                        <div class="row py-2">
                            <div>
                                <h4>MY ACCOUNT</h4>
                                <ul class="list-group">
                                    <li class="fList"><a href="SignUp.aspx">Register</a></li>
                                    <li class="fList"><a href="#">My Cart</a></li>
                                    <li class="fList"><a href="#">Order History</a></li>
                                    <li class="fList"><a href="buy.html">Payment</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- footer column 3 end -->
                    <!-- footer column 4 start -->
                    <div class="col-md-4">
                        <!-- row start -->
                        <div class="row py-2 ">
                            <div>
                                <h4>OUR MAIN STORES</h4>
                                <button type="button" onclick="changeCityIsrael()" class="btn btn-default">Israel</button>
                                <button type="button" onclick="changeCityAustralia()" class="btn btn-default">Australia</button>
                                <ul class="list-group">
                                    <li class="fList"><i class="fa fa-map-marker" aria-hidden="true"></i><span id="adress">2990 Haifa , Israel</span></li>
                                    <li class="fList"><i class="fa fa-phone" aria-hidden="true"></i><span id="phone">025-2839341</span></li>
                                    <li class="fList"><a href="info@simplefashion.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>info@boardseek.com</a></li>
                                </ul>
                            </div>
                            <!-- row end -->
                        </div>
                        <!-- footer column 4 end -->
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="container-fluid bg-white py-3">
            <div class="container">
                <div class="container-">
                    <p class="pull-right">
                        <a href="#">
                            <img src="images/arrow-up.png" /></a>
                    </p>
                    <p class="text-muted">&copy; 2017 BoardSeek.com &middot; <a href="Default.aspx" class="text-muted">Home</a> &middot; <a href="#" class="text-muted">About</a> &middot; <a href="#" class="text-muted">Contact</a> &middot;</p>
                </div>
            </div>
        </div>

    </footer>
    <!-- Footer end -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/MenuHover.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
