﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Drawing;

public partial class SignIn : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        // If the details are changed and we refresh the page, give us new details.
        if (!IsPostBack)
        {
            if (Request.Cookies["Email"] != null && Request.Cookies["Pwd"] != null)
            {
                Email.Text = Request.Cookies["Email"].Value;
                Password.Attributes["value"] = Request.Cookies["Pwd"].Value;
                CheckBox1.Checked = true;
            }
        }

    }

    protected void ButLogIn_Click(object sender, EventArgs e)
    {
        String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;
        using (SqlConnection con = new SqlConnection(CS))
        {

            SqlCommand cmd = new SqlCommand("select * from Customers where Email='" + Email.Text +
                "' and Password='" + Password.Text + "'", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);


            if (dt.Rows.Count != 0)
            {
                Session["EMAIL"] = dt.Rows[0]["Email"].ToString();
                Session["CUSTID"] = dt.Rows[0]["CustID"].ToString();
                Session["PASSWORD"] = dt.Rows[0]["Password"].ToString();

                if (CheckBox1.Checked) //In case the checkbox is checked we will create cookies in order to remember the login details
                {
                    Response.Cookies["Email"].Value = Email.Text;
                    Response.Cookies["Pwd"].Value = Password.Text;

                    Response.Cookies["Email"].Expires = DateTime.Now.AddDays(15);
                    Response.Cookies["Pwd"].Expires = DateTime.Now.AddDays(15);

                }
                else //If not sub 1 from the expiry date of the last user
                {
                    Response.Cookies["Email"].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies["Pwd"].Expires = DateTime.Now.AddDays(-1);

                }

            }
            else //if password and Email do not match we have error message
            {
                lblError.ForeColor = Color.Red;
                lblError.Text = "Invalid Email or Password !";
            }

            string UserType;
            UserType = dt.Rows[0][8].ToString().Trim();

            //now we are checking if the user is Admin or not before loading the next page

            if (UserType == "U")
            {
                var row = dt.Rows[0];
                Session["EMAIL"] = Email.Text;
                Session["Fname"] = row["Fname"];
                if (Request.QueryString["rurl"] != null)
                {
                    if(Request.QueryString["rurl"] == "cart")
                    {
                        Response.Redirect("~/Cart.aspx");
                    }
                }
                else
                {

                }
                Response.Redirect("~/UserHome.aspx"); //Redirect if the login is OK
            }

            if (UserType == "A")
            {
                var row = dt.Rows[0];
                Session["EMAIL"] = Email.Text;
                Session["Fname"] = row["Fname"];
                Response.Redirect("~/AdminHome.aspx"); //Redirect if the Admin login is OK
            }
        }



    }
}