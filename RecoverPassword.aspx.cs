﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;

public partial class RecoverPassword : System.Web.UI.Page
{
    String CS = ConfigurationManager.ConnectionStrings["bsdbConnectionString1"].ConnectionString;
    String GUIDvalue;
    DataTable dt = new DataTable();
    String Email;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            GUIDvalue = Request.QueryString["Uid"];
            if (GUIDvalue != null)
            {
                SqlCommand cmd = new SqlCommand("Select * from ForgotPassRequests where id='" + GUIDvalue + "'", con);
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                if (dt.Rows.Count != 0)
                {
                    Email = Convert.ToString(dt.Rows[0][1]);
                }
                else
                {
                    lblMsg.Text = "Your password reset link has expired or invalid.";
                    lblMsg.ForeColor = Color.Red;
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            if (dt.Rows.Count != 0)
            {
                tbNewPass.Visible = true;
                tbRetypePass.Visible = true;
                lblPassword.Visible = true;
                lblRetypePass.Visible = true;
                btRecPass.Visible = true;
            }
            else
            {
                lblMsg.Text = "Your password reset link has expired or invalid.";
                lblMsg.ForeColor = Color.Red;
            }
        }
    }

    protected void btRecPass_Click(object sender, EventArgs e)
    {
        if (tbNewPass.Text != "" && tbRetypePass.Text != "" && tbNewPass.Text == tbRetypePass.Text)
        {
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("update Customers set Password='" + tbNewPass.Text + "'where Email = '" + Email + "'", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("delete from ForgotPassRequests where  Email = '" + Email + "'", con);
                cmd2.ExecuteNonQuery();
                Response.Redirect("~/SignIn.aspx");
            }
        }
    }
}